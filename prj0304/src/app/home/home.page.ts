import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "App motos";

  cards = [
  {
    marca:"Ducati",
    titulo: "Ducati Diavle 1260 s",
    subtitulo : "Preço: R$25.000,00",
    conteudo: " OHC, Monocilíndrico 4 tempos, arrefecido a ar.",
    foto1: "https://images.ctfassets.net/x7j9qwvpvr5s/6iIiChCivz5DXQgDehHCHN/cf94d9fdecf672c25e3bc702f352e372/Diavel-1260-S-Glossy-Matt-Black-MY20-Model-Preview-1050x650.png",
    foto2: "https://img.r7.com/images/nova-diavel-1260-ganha-banho-de-loja-da-ducati-divulgacao-28112019184835310?dimensions=660x360&",
    foto3:"https://production.autoforce.com/uploads/picture/image/86438189/comprar-diavel-1260-s-2020-f0a26689-92ba-4f4f-90ef-1b114f8bc0a0_d1ecc41411.jpg",
    estrelas: 9

  },
  {
     marca: "Yamaha",
     titulo: "Yamaha XTZ lander 250",
     subtitulo: "Preço: R$12.000,00",
     conteudo:" Motor: Monocilíndrico de quatro tempos, com duas válvulas, OHC, refrigeração a ar",
     foto1: "https://http2.mlstatic.com/D_NQ_NP_808787-MLB46924873566_072021-O.jpg",
     foto2: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMmsStlCv-h0by_dpeqUMtBxgp0jJ8Liyv7FRc3bmv4M5J6D5kXyP9yjm8ceeXSfl0lys&usqp=CAU",
     foto3: "https://motos2023.com.br/wp-content/uploads/2022/02/yamaha-xtz-250-lander-abs-2022-3.jpg",
     estrelas: 6
  },
  {
    marca:"Suzuki",
    titulo:"Suzuki Boulevard m800",
    subtitulo: " Preço: R$18.000,00",
    conteudo:" OHC, Monocilíndrico 4 tempos, arrefecido a ar.",
    foto1: "https://img.estadao.com.br/wp/jornal-do-carro/files/2010/08/boulevard.JPG",
    foto2:"https://bestcars.com.br/bc/wp-content/uploads/2016/07/Suzuki-M800.jpg",
    foto3: "https://www.moto.com.br/img/2007/04/25/img6135-1177504302-v580x435.jpg",
    estrelas: 10
  },
  {
    marca:"Honda",
    titulo:"Honda CB 1000R"   ,
     subtitulo: "Preço: R$20.000,00" ,
    conteudo:" OHC2, Monocilíndrico 2 tempos, arrefecido a ar.",
    foto1: "https://www.honda.com.br/motos/sites/hda/files/2018-12/slide01.jpg",
    foto2: "https://i0.wp.com/www.motozoo.com.br/wp-content/uploads/2019/07/honda-reveals-neo-sports-cafe-project-early.jpg?ssl=1",
    foto3: "https://www.honda.pt/content/dam/central/motorcycles/colour-picker/street/cb1000r/cb1000r_std_2018/r-381c_candychromospherered/cb1000r_std_2018_r-381c_candychromospherered.png/jcr:content/renditions/fb_r.png",
    estrelas: 10
  }
  ];

  
  

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor() {}

}
